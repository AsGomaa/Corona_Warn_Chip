@echo off

call ./env.bat

@REM echo uploading ui
@REM ampy rmdir /ui
@REM ampy put ./src/ui /ui

REM echo uploading bitmaps
REM ampy rmdir /bitmaps
REM ampy put ./src/bitmaps /bitmaps

@REM echo uploading store
@REM ampy rmdir /store
@REM ampy put ./src/store /store

@REM echo uploading drivers
@REM ampy rmdir /drivers
@REM ampy put ./src/drivers /drivers

echo uploading enf
ampy rmdir /enf
ampy put ./src/enf /enf

@REM echo uploading networking
@REM ampy rmdir /networking
@REM ampy put ./src/networking /networking

@REM echo uploading main and power 
@REM ampy put ./src/main.py /main.py
@REM ampy put ./src/power.py /power.py

@REM ampy put ./src/atask.py /atask.py
ampy put ./src/scheduler.py /scheduler.py

echo starting serial console
call ./console.bat
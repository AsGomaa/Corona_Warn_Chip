
""" implements functions related to power management, sleeping and waking """
import scheduler
import uasyncio as asyncio
from machine import Timer
from ui.router import transition
from store.power import saveWakestate, loadWakestate

inactivitytimer = Timer(6)

def inactivityTimer( wakestate , period=10000 ):
     #pass # temorary disable deep sleep during developmnet
     inactivitytimer.init(period=period, mode=inactivitytimer.ONE_SHOT, callback=detectInactivity( wakestate ))

def detectInactivity( wakestate ):
    """ 
    puts the device to sleep
    @wakestate: string name of the state to wake up in
    """
    def closure( inactivitytimer ):
        transition("sleep", wakestate)
    return closure


def deepsleep( wakestate="welcome" ):
    """ 
    Enters a deep sleep state, 
    where buttonA or buttonC press can wake the device up 
    @wakestate String pagename to show when woken
    """

    import esp32
    from machine import Pin, deepsleep
   
    buttonA = Pin(39, mode = Pin.IN)
    buttonC = Pin(37, mode = Pin.IN)

    saveWakestate( wakestate ) 

    #level parameter can be: esp32.WAKEUP_ANY_HIGH or esp32.WAKEUP_ALL_LOW
    esp32.wake_on_ext0( pin = buttonA, level = esp32.WAKEUP_ALL_LOW )
    esp32.wake_on_ext1( (buttonC,) , level = esp32.WAKEUP_ALL_LOW )

    # sleep fo 10 seconds
    deepsleep(10000)

def wake(): # async ? 
    """ Based on the wake reason, actions or transitions are executed """
    from machine import wake_reason, TIMER_WAKE, EXT0_WAKE, EXT1_WAKE
    from ui.router import transition


    wakereason = wake_reason()

    if wakereason == TIMER_WAKE:
        """ Woke by timeout """
        print("running backgournd tasks")
        # load and run the tasks that are scheduled to run now
        scheduler.load()
        asyncio.run(scheduler.run())
        #asyncio.run_until_complete(scheduler.run())
        scheduler.save()
        # sleep fo 10 seconds
        print("entering deep sleep")
        deepsleep()

    elif wakereason == EXT0_WAKE:
        """ Woke by buttonA press """
        #show the last page
        wakestate = loadWakestate() 
        print("woke from deepsleep by buttonA press, transitioning to ", wakestate)
        transition( wakestate, None )

    elif wakereason == EXT1_WAKE:
        """ Woke from deepsleep by buttonC press """
  
        #show the last page
        wakestate = loadWakestate()
        print("woke by buttonC press transitioning to ", wakestate)
        transition( wakestate, None )

    else:
        """ inital power on """
        # schedule tasks to run when waking from deepsleep by timer
        scheduler.clear()
        scheduler.schedule("networking.download","downloadfile","teks.json", seconds=40)
        scheduler.schedule("atask","foo","moo", seconds=30)
        #scheduler.schedule("enf.transmission","schedulingTransmission",True, minutes=2)
        #scheduler.schedule("networking.download","downloadfile","teks.json", minutes=1)
        #scheduler.schedule("enf.risk_calculations","calculateRisk","teks.json",  hours=24)
        scheduler.save()

        # show the welcome page
        transition( "welcome", None )


"""
This stores the risk riskstate of the device
"""

riskstate = {
    "startdate":1602568849, #timestamp of when we turn on the device
    "risklevel":1,
    "timestamp": 1602568849, # timestamp of the latest tek keys downloaded
    "lowrisklevelmin": 0,
    "lowrisklevelmax": 15,
    "highrisklevelmin": 15,
    "highrisklevelmax": 9999,
}

# mutations
def startdate( *argv ):
    """
    the time stamp when the device was first used
    """
    if len(argv):
        riskstate["startdate"] = argv[0]
    return riskstate["startdate"]

def risklevel( *argv ):
    """
    the current risk risklevel
    """
    if len(argv):
        riskstate["risklevel"] = argv[0]
    return riskstate["risklevel"]

def timestamp( *argv ):
    """
    the timestamp of the current risk risklevel
    """
    if len(argv):
        riskstate["timestamp"] = argv[0]
    return riskstate["timestamp"]

#actions
def save( filename='riskstore.json' ):
    """write risk riskstate to flash"""
    import json
    with open(filename, 'w') as file:
        json.dump(riskstate, file)

def load( filename='riskstore.json' ):
    """load risk riskstate from flash"""
    import json 
    try:
        with open(filename, 'r') as file:
            global riskstate
            riskstate = json.load(file)
    except Exception :
        pass

#getters
def risk():
    """ provides the risk level to the user """
    if risklevel() < riskstate["lowrisklevelmax"] and risklevel() >= riskstate["lowrisklevelmin"]:
        return 1
    if risklevel() <= riskstate["highrisklevelmax"] and risklevel() >= riskstate["highrisklevelmin"] :
        return 2
    return 0
    
def startdateString():
    """ provides the start date as a formatted string """
    # pylint: disable=import-error
    from utime import localtime
    # pylint: enable=import-error
    # linux uses an epoch of 1976, where micropython uses 2000.
    time_diff = 946684800 #The 1976 epoch timestamp on 2000, 01, 01 00:00:00
    year, month, day, hours, minutes, seconds, weekday, yearday = localtime( startdate() - time_diff )
    return "{0}-{1}-{2}".format(year, month, day)

def timestampString():
    """ provides the current risk timestamp as a formatted string """
    # pylint: disable=import-error
    from utime import localtime
    # pylint: enable=import-error
    # linux uses an epoch of 1976, where micropython uses 2000.
    time_diff = 946684800 #The 1976 epoch timestamp on 2000, 01, 01 00:00:00
    year, month, day, hours, minutes, seconds, weekday, yearday = localtime( startdate() - time_diff )
    return "{0}-{1}-{2} {3}:{4:02d}".format(year, month, day, hours, minutes)

load() # the riskstate set from flash, when the library is first called



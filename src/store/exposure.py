

def readexposure(filename = r"networking/receivedBeacons.json"):
    """
    read the receiced exposure from json file
    """

    import json
    if file_exists(filename):
        try:
            with open(filename, 'r') as json_file:
                data = json.load(json_file)
                numExposure = len(data)
                return numExposure
        except Exception:
            pass
    else:
        return False

    

def check (dictionary):
    """ 
    checks for only 14 days exposure data
    """

    from collections import OrderedDict

    keylist = list(dictionary)

    if len(dictionary) == 15:
        tempentry = [dictionary[keylist[14]][0],dictionary[keylist[14]][1] + dictionary[keylist[0]][0]]
        dictionary[keylist[14]] = tempentry
        dictionary.pop(keylist[0])
        return dictionary
    else:
        return dictionary
        
        
    

def file_exists(fname):
    """
    checks the existance of a file
    """

    try:
        with open(fname):
            pass
    except Exception:
        return False
    else:
        return True
   
    
def save( filename='exposurestore.json', date = None):
    """
    write exposure to flash
    save the exposure in dictionary with
        key - date
        value - cumulative exposure count,current day exposure count
    """
    from collections import OrderedDict
    import json
    from utime import localtime

    #from datetime import datetime
    #date = datetime.today().strftime('%Y-%m-%d')
    
    
    exposure = OrderedDict() 
    date = str(localtime()[:3]).replace("(","[").replace(")","]")

    if file_exists(filename):
        try:
            with open(filename, 'r') as file:
                temp = json.load(file)
                keylist = list(temp.keys())
                
                #print( str(date) +"==" + str(keylist[-1]) +"/" + str(type(date)) + "/" + str(type(keylist[-1])) )

                if not date == keylist[-1]:
                    temp[date] = [ readexposure() , readexposure() - temp[keylist[-1]][0] ]
                    print("date changed")
                
                else:
                    temp[date] = [ readexposure() , ( readexposure()-temp[date][0] ) + temp[date][1] ]
                    print("same date")

                #exposure = temp  
                exposure = check(temp)

        except Exception as e:
            print(e)

        try:  
            with open(filename, 'w') as file:
                json.dump(exposure, file)
        except Exception as e:
            print(e)
  
    else:
        try:
            with open(filename, 'w') as file:
                exposure[date] = [ readexposure() , readexposure() ]
                json.dump(exposure, file)
        except Exception as e:
            print(e)


def load( filename='exposurestore.json' ,date = None):
    """
    load exposure from flash
    """

    import json 
    from utime import localtime

    date = str(localtime()[:3]).replace("(","[").replace(")","]")

    #from datetime import datetime
    #date = datetime.today().strftime('%Y-%m-%d')

    if file_exists(filename):
        try:
            with open(filename, 'r') as file:
                exposuredict = json.load(file)
                exposurestate = exposuredict[date]
                return exposurestate
                
        except Exception as e:
            print(e)
    else:
        return "Not found"
    
        


if __name__ == "__main__": 

    readexposure()
    print("Exposure Read  \n")
    save()
    print("Saved  \n")
    load()
    print('load')


    



"""
This stores the entered  TAN

"""

def saveTan( tan, filename='tan.dat' ):
    """
    write tan to flash
    """
    import json
    try:
        with open(filename, 'w') as file:
            json.dump(tan, file)
    except Exception:
        pass

def loadTan( filename='tan.dat' ):
    """
    load tan from flash
    """
    import json 
    try:
        with open(filename, 'r') as file:
            tan = json.load(file)
            return str(tan)
    except Exception :
        pass

def clearTan( filename='tan.dat' ):
    """
    clear tan from flash
    """

    import json
    emptytan = ""
    try:
        with open(filename, 'w') as file:
            json.dump(emptytan, file)
    except Exception :
        pass
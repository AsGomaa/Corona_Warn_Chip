import uasyncio
async def foo( moo="" ):
    print('start foo',moo)
    await uasyncio.sleep(1)
    print('ending foo',moo)

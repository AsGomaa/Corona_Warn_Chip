import json
import utime
import ubinascii
import uasyncio as asyncio
from enf.dateandtime import get_timestamp_from_interval
from enf.newencryption import create_list_of_rpis_for_interval_range, derive_rpi_key
from store.risk import risklevel
from enf.jsontokenizer import Tokenizer, generator


# Parameters Section
# risk vlaue:
NUM_RISK_THRESHOLD   = 15
MINIMUM_RISK_SCORE = 11
# duration:
DURATION_THRESHOLD     = 2
EXPOSURE_RISK         = 1
# attenuation:
ATTENUATION_THRESHOLD = 73
ATTENUATION_RISK      = 2
# number of days:
LAST_EXPOSURE = {"7": 5, "6": 5, "5": 5, "4": 5,"3": 5, "2": 5, "1": 5,"0": 5} 
# transmission
TRANSMISSION = {"1" : 0, "2" : 0, "3" : 3, "4" : 4, "5" : 5, "6" : 6, "7" : 7, "8" : 8}
RISK_SCORE_DIVISOR = 50

def total_risk_score(startInterval, duration, tr_risk_lvl):  # receive the count use it in line combined_risk_score(duration, attenuation)
    '''
    Stores the "total risk" in a dictionary and returns a flag to preceed with the risk calculations

    Args:
        startInterval (int): the interval in from which the TEK generated.
        duration (list): the durations of exposure to each RPI belonging to a TEK.
        attenuation (list): the average attenuation vlaues of each received RPI belonging to a TEK.
        tr_risk_lvl (int): a constant specified by the health authorities. 

    Returns: 
        A boolen that is True if the total_risk is greater than a threshold and Flase otherwise
    '''
    # TODO initialize time properly in the start 
    # Calculating the number of days since the encounter
    days = (utime.time() - get_timestamp_from_interval(startInterval)) // (3600 * 24) 
    # Calculating the "total risk" parameters
    total_risk = (LAST_EXPOSURE[str(days//2)]  * TRANSMISSION[str(tr_risk_lvl)] * exposureRisk(duration) * ATTENUATION_RISK)
    print("total risk record: {}".format(total_risk_record))
    # Storing the "total risk" parameters and returning a flag
    if total_risk > MINIMUM_RISK_SCORE:
        total_risk_record(total_risk)
        return False
    else:
        return True


def combined_risk_score(startInterval, duration, tr_risk_lvl):
    '''
    calculates the overall numerical risk value. 

    Args:
        startInterval (str): an integer for the interval during which the TEK is generated.
        duration (list): the period of time the device encounters a positive user
        tr_risk_lvl (str): the transmission risk level (meaning specified by the app)

    Returns:
        the overall numerical risk value.
    '''
    if not total_risk_score(startInterval, duration, tr_risk_lvl):
        exposure_score = ((duration[0] - 1) + (duration[1] - 1) * 0.5) * 5 
        with open('total_risk_record.json', 'r') as f:
            risk_record = json.load(f)
            normalized_total_risk = risk_record["maximum"] / RISK_SCORE_DIVISOR

        combined_risk = exposure_score * normalized_total_risk
        return combined_risk

    else:
        return 0 


def exposureRisk(duration):
    """ 
    return the "exposure risk" and discards encounters the lasted less than a predefined threshould 
    """
    return 0 if sum(duration) < DURATION_THRESHOLD else EXPOSURE_RISK 
    

def total_risk_record(total_risk):
    '''
    Stores the maximum total risk score in a json file in the SD card

    Args: 
        total_risk (int): the total risk score
    '''
    # TODO: operate in the SD card
    if file_exists('total_risk_record.json'): 
        with open('total_risk_record.json', 'r') as f:
            risk_record = json.load(f)
            # Store the maximum total risk in the file
            if total_risk > risk_record["maximum"]:
                risk_record["maximum"] = total_risk
            with open('total_risk_record.json', 'w') as f:
                json.dump(risk_record, f)
    # Create the file if it did not exist before    
    else:
        risk_record = {"maximum": total_risk}
        with open('total_risk_record.json', 'w') as f:
            json.dump(risk_record, f)


def reset_total_risk_record():
    '''
    resets the value of the total risk score to 0
    '''
    if file_exists('total_risk_record.json'):
        with open('total_risk_record.json', 'w') as fw:
            risk_record = {}
            risk_record["maximum"] = 0
            json.dump(risk_record, fw)



def reencrypt(tek, startInterval):
    '''
    re-encrypt TEK to produce the RPIs generated on that day.

    Args: 
        tek (str): a single diagnoisis key downloaded from the server
        startInterval (str): the starting interval during which the passed TEK was valid

    reuturns: 
        a list of lists each list element contains 144 rpis generated in a given day.
    ''' 
    return create_list_of_rpis_for_interval_range(derive_rpi_key(ubinascii.unhexlify(tek)),
                                                    startInterval, 144, hexlif = True)


def matching(tek, TRANSMISSION_lvl, startInterval): # ADDED
    '''
    matches the downloaded data of infected people with the received BT signals and calculates the
    risk

    Args:
        tek (str): the downloaded diagnosis key
        TRANSMISSION_lvl (str): the transmission risk level (meaning specified by the app)
        startInterval (str): the interval specifying the time window during which the TEK is generated

    Returns:
        A boolean that is True if the risk is high; False otherwise 
    '''
    # Check the existance of the receivedBeacons.json file
    if file_exists('receivedBeacons.json'):
        with open('receivedBeacons.json', 'r') as f:
            storage = json.load(f) 
            # Extracts matches between the reencrypted dowloaded diagnosis keys and the 
            # received Bluetooth data and stores them into a list. 
            matched_set = list(set(storage.keys()).intersection(set(reencrypt(tek, startInterval))))
        # Calculate the risk if matches have been found 
        if len(matched_set) != 0:
            for rpi in matched_set:
                # Extract the start interval to calulate the "combined risk score"
                start_interval = storage[rpi][2] 
                # Calculating the overall risk 
                numerical_risk = combined_risk_score(start_interval, storage[rpi][2], TRANSMISSION_lvl)
                # ADDED for asynchronous operation
                # await asyncio.sleep(0.001)
                # Check if the overall risk passes the numerical risk threshold
                if numerical_risk >= NUM_RISK_THRESHOLD: 
                    # Store the numerical risk 
                    print("Risk increased")
                    risklevel(numerical_risk)
                    break
        # Flag
        return True
                    
    else: 
        return False


def calculateRisk(): # ADDED

    tokenizer = Tokenizer()

    def tokenizeDescendants(name, gen):
        yield from tokenizer.tokenizeValue(gen, True)

    counter = 0
    tek = ""
    lvl =0
    interval=0

    if file_exists("teks.json"):

        print("the file teks exists")
        for tokens, value in tokenizer.tokenizeValuesNamed(["TemporaryExposureKey","transmissionRiskLevel","rollingStartIntervalNumber"], tokenizeDescendants, generator() ):

            if counter == 0:
                tek = value
                counter += 1

            elif counter == 1:
                lvl = value
                counter += 1

            elif counter == 2:
                interval = value
                counter = 0
            print("enter matching")
            matching(tek, lvl, interval)
            # await asyncio.sleep(0) # ADDED
            if matching(tek, lvl, interval):
                break
            
            print("finished processing")
            reset_total_risk_record()  


def file_exists(fname):
    '''checks the existance of a file
    
     Args:
      fname (str): A string containing the file name with the extension

     Returns:
      A boolean that is True if the file exists; False otherwise 
    '''
    try:
        with open(fname):
            pass
    except Exception:
        return False
    else:
        return True
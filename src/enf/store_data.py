import json
import utime
import ubinascii
from enf.risk_calculations import file_exists
from enf.dateandtime import get_interval_from_utc_timestamp


def dict_setter(storage, encrypted_data):
    '''
    sets the value of the dicitonary containig the received data via Bluetooth [rpi, aem, rssi]

    Args:
        storage (dict): a dictionary containig the the rpi as keys and the received encrypted data as value
        {rpi: [aem, rssi, interval_num, timeStamp, counter]}
        encrypted_data (list): the received data via blutooth

    Returns: 
        storage (dict): the updated storage dictionary
    '''
    # Threshold of a strong BT signal
    rssi_strong = -89 
    # Threshold of a weak BT signal
    rssi_weak   = -93 
    # Adding the interval number to the stored data 
    encrypted_data.append(utime.time() // (10 * 60))
    # Adding the time to the stored data  
    encrypted_data.append(utime.time())
    # Deciding to consider the received BT signal based on its strength
    # any received signal level below rssi_weak is discarded
    if encrypted_data[2] >= rssi_weak: 
        if encrypted_data[2] < rssi_strong:
            # strength_counter = [storngCount, mediumCount]
            strength_counter = [0, 1] 
        else:
            strength_counter = [1, 0] 
        # if the received RPI is already stored
        if ubinascii.hexlify(encrypted_data[0]).decode() in storage.keys(): 
            storage[ubinascii.hexlify(encrypted_data[0]).decode()][1] = 0.5 * storage[ubinascii.hexlify(encrypted_data[0]).decode()][1] + 0.5 * encrypted_data[2]
            # ensuring that each count corresponds to a 5 minute period accrodign to CWA documentation
            if utime.time() - storage[ubinascii.hexlify(encrypted_data[0]).decode()][3] > 290:
                # Upadating the strngths counter
                storage[ubinascii.hexlify(encrypted_data[0]).decode()][4] = [a + b for a, b in zip(storage[ubinascii.hexlify(encrypted_data[0]).decode()][4], strength_counter)]
                storage[ubinascii.hexlify(encrypted_data[0]).decode()][3] = 0     
        else:
            # Storing a new RPI with its corrsponding data.
            # rpi: [aem, rssi, interval_num, timeStamp, counter]
            storage[ubinascii.hexlify(encrypted_data[0]).decode()] =  encrypted_data[1::]
            storage[ubinascii.hexlify(encrypted_data[0]).decode()].append(strength_counter)

    return storage


def store(encrypted_data):
    '''
    stores the received encrypted data via Bluetooth 

    Args:
        encrypted_data (list): the received data via blutooth
    '''
    # Checking the existance of the receivedBeacons.json file
    if file_exists('receivedBeacons.json'): 
        with open('receivedBeacons.json', 'r') as f:
            print("the file now exists")
            storage = json.load(f)
    # Creating the receivedBeacons.json file to store the received Bluetooth data in it
    else:
        print("creating a new dict")
        storage = {}
    # Adding the incoming data to the storage dictionary 
    storage = dict_setter(storage, encrypted_data)
    # Removing outdated elements from the dictionary
    update_storage(storage, 2, 'receivedBeacons.json')


def update_storage(storage, index, filename):
    '''
    delete RPIs that are stored for more than 14 days and stores the the beacons in the file
    receivedBeacons.json.

    Args:
        storage (dict): a dictionary containig the the rpi as keys and the received encrypted data as value
        {rpi: [aem, rssi, interval_num, timeStamp, counter]}
        index (int): the index of the vlaue that needs to be updated
        filename (string): the name of the file that containes the data that need to updated
    '''
    # Selecting the point of time before which the stored data is discarded for being too old
    keep_from = get_interval_from_utc_timestamp(utime.time(), server = False) - 2016
    # Applying the update criteria 
    temp = dict({ k:v for k, v in storage.items() if (v[index] >=  keep_from)})
    # Saving the updated dictionary 
    with open(filename, 'w') as f1:
        json.dump(temp, f1)

# TODO: recalibration of the rssi thresholds.
import urequests as requests
import ujson as json 
from utime import mktime, time
from dateandtime import get_interval_from_utc_timestamp
from os import urandom
from transmission import storeTEK
from risk_calculations import file_exists
from networking.teleTANupload import checkTeleTAN

# constants
TRL_PROFILE = [5, 6, 8, 8, 8, 5, 3, 1, 1, 1, 1, 1, 1, 1]  # 0: today, 1: yesterday, ...

NUMBER_OF_UPLOAD_DAYS = 14
NUMBER_OF_UPLOADS_PER_DAY_MIN = 5
NUMBER_OF_UPLOADS_PER_DAY_MAX = 20
ENIN_ONE_DAY = 144


def dummyKeys():
    ''' 
    return a set of dummy keys for dummy uploads that protect the users with positive diagnosis 
    from melicious attackers who can monitor the data traffic
    '''
    keys    = [{"TemporaryExposureKey":urandom(16)} for _ in range(16)]
    storage = {"diagnosiskeys": keys} 
    if file_exists('dummy_teks.json'):
        with open('dummy_teks.json', 'w') as f:
            json.dump(storage, f)

    
def verifyTan(teleTeleTAN):
    '''
    uploads the teleTAN to the verification server for verification. if the teleTAN is verified, the 
    function returns the registeration token.
    '''
    url  = " " # TODO: the url of the verification server.
    body = {"key": str(teleTeleTAN), "keytype": "TELETAN"} 
    requests.post(url, data = body)


def requestTAN(regToken):
    '''
    uploads the registeration token to the verification server to request the TAN that is uploaded
    along with the diagnosis keys.
    '''
    url  = " " # TODO: the url of the verification server.
    body = {"registrationToken": regToken} 
    requests.post(url, data = body)


def prepareData():
    '''
    generates a file containing the available diagnosis keys of the last 14 days
    '''
    today_stamp = mktime(todaysDate()) 
    last_enin = get_interval_from_utc_timestamp(today_stamp, server = False) - 144
    

    with open('stored_teks.json', 'r') as f:
        storage = json.load(f) 
    num_keys = len(storage["diagnosisKeys"])

    publish = {
    "startTimestamp":today_stamp - 86400 * num_keys,
    "endTimestamp": today_stamp ,
    "region": "DE",
    "batchNum": 1,
    "batchSize": num_keys,
    "signatureInfos": {"verification_key_version": "v1", 
    "verification_key_id": "262", 
    "signature_algorithm": "1.2.840.10045.4.3.2"},
    "keys": list()
    }

    for pos in range(num_keys - 1):
        key        = storage["diagnosisKeys"][pos]
        trl        = TRL_PROFILE[pos + 1]
        start_enin = last_enin - (pos + 1) * ENIN_ONE_DAY
        duration   = ENIN_ONE_DAY

        publish["keys"].append({"TemporaryExposureKey": key, "transmissionRiskLevel": trl, "validity":{"rollingStartIntervalNumber":start_enin, "rollingPeriod": duration}})
    
    with open('publishFile.json', 'w') as f:
        json.dump(publish, f)

def exportData(TAN):
    '''
    uploads the diagnosis keys to the CWA server. 
    '''
    if file_exists('publishFile.json'):
        with open('publishFile.json', 'r') as f:
            diagkeys = json.load(f)
            url  = " " # TODO: CWA server address
            requests.post(url, headers = TAN, files = diagkeys)

def todaysDate():
    '''
    returns todays date in the form 
    '''
    url = 'http://worldtimeapi.org/api/timezone/CET'
    response = requests.get(url)
    parsed = response.json()["datetime"]
    y,m,d = int(parsed[0:4]), int(parsed[5:7]), int(parsed[8:10])
    return y,m,d,0,0,0,0,0
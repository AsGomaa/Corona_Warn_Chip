import utime
import machine


interval_length_minutes = 10  # 10 minutes per interval
tek_rolling_period = 144  # 24*60//10 - 24 hours per day, 60 minutes per hour, 10 minutes per interval
rtc = machine.RTC()

def get_timestamp_from_interval(interval_number):
    ''' returns the Unix time stamp from interval number
    
    Args:
        interval_number (int): the number of 10-minute intervals since 01.01.1970 00:00:00

    Returns:
        (int): utc time stamp (time in seconds since 01.01.1970 00:00:00)
    '''
    return interval_number * interval_length_minutes * 60  


def get_interval_from_utc_timestamp(time_stamp, server = True):
    ''' returns the Unix time stamp from interval number
    
    Args:
        utc_timestamp (int): utc time stamp (time in seconds since 01.01.1970 00:00:00)
        server (boolean): True if the time stamp is received from the server; Flase if the time 
        stamp is generated using utime library. 

    Returns:
        (int): the number of 10-minute intervals since 01.01.1970 00:00:00
    '''
    if server: 
        return time_stamp // (interval_length_minutes * 60)  
    else:
        return (time_stamp + 946684800) // (interval_length_minutes * 60) 
        

def get_datetime_from_utc_timestamp(utc_timestamp, server = True):
    ''' returns the date and time from the Unix time stamp

    Args:
        utc_timestamp (int): utc time stamp (time in seconds since 01.01.1970 00:00:00)
        server (boolean): True if the time stamp is received from the server; Flase if the time 
        stamp is generated using utime library. 


    Returns:
        (tuple): (year, month, day, hours, minutes, seconds, day of the week, day of the year)
    '''
    if server:
        ctr_stamp = utc_timestamp - 946684800 
        return utime.localtime(ctr_stamp)
    else:
        return utime.localtime(utc_timestamp)


def get_timestamp_from_utc_datetime(utc_datetime):
    ''' returns the Unix time stamp from the datetime tuple

    Args:
        utc_datetime (tuple): (year, month, day, hours, minutes, seconds, day of the week, day of the year)

    Returns:
        (int): utc time stamp (time in seconds since 01.01.1970 00:00:00) 
    '''
    return int(utime.mktime(utc_datetime)) + 946684800


def get_string_from_datetime(date_time):
    ''' returns the datetime as a string 

    Args:
        utc_datetime (tuple): (year, month, day, hours, minutes, seconds, day of the week, day of the year)

    Returns:
        (str): utc datetime in the form: YYYY-MM-DD HH:MM:SS 
    '''
    return "{}-{:0>2d}-{:0>2d} {}:{}:{}".format(date_time[0],date_time[1],date_time[2],
                                     date_time[3], date_time[4], date_time[5])

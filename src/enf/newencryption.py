from enf.res import sha256
from enf.res.hmac import *
import sys
import uhashlib
import ucryptolib       
from enf.res.aes import *
from enf.res.blockfeeder import *
from enf.res.util import *
import os
import ubinascii


#############
# HKDF setup 
#############
import struct
from enf.res.sha256 import *
hashing = Sha256

def hkdf_extract(salt, input_key_material):
	'''
	Extract a pseudorandom key suitable for use with hkdf_expand
	from the input_key_material and a salt using HMAC with the
	provided hash (default SHA-512).
	salt should be a random, application-specific byte string. If
	salt is None or the empty string, an all-zeros string of the same
	length as the hash's block size will be used instead per the RFC.
	
	See the HKDF draft RFC and paper for usage notes.
	'''
	hash_len = 16 #hashlib.sha256.digest_size
	if salt == None or len(salt) == 0:
		salt = bytearray((0,) * hash_len)
	return new1(salt, input_key_material, hashing).digest()

def hkdf_expand(pseudo_random_key, info=b"", length=32, hash=hashing):
	'''
	Expand `pseudo_random_key` and `info` into a key of length `bytes` using
	HKDF's expand function based on HMAC with the provided hash (default
	SHA-512). See the HKDF draft RFC and paper for usage notes.
	'''
	hash_len = hash().digest_size
	length = int(length)
	if length > 255 * hash_len:
		raise Exception("Cannot expand to more than 255 * %d = %d bytes using the specified hash function" %\
			(hash_len, 255 * hash_len))
	blocks_needed = length // hash_len + (0 if length % hash_len == 0 else 1) # ceil
	okm = b""
	output_block = b""
	for counter in range(blocks_needed):
		output_block = new1(pseudo_random_key, (output_block + info + bytearray((counter + 1,))),\
			hash).digest()
		okm += output_block
	return okm[:length]

#####################################################
# Encryption/Decryption functions
#####################################################
interval_length_minutes = 10
tek_rolling_period=144

def generate_tek(number_of_bytes = 16):
	return os.urandom(number_of_bytes)


def en_interval_number(timestamp_seconds):
    return timestamp_seconds // (60 * interval_length_minutes)


def encoded_en_interval_number(enin):
    return struct.pack("<I", enin)


def derive_rpi_key(tek):
    key = hkdf_extract(None, tek)
    key = hkdf_expand(key, "EN-RPIK".encode("UTF-8"), length = 16)
    return key


def derive_aem_key(tek):
    key = hkdf_extract(None, tek)
    key = hkdf_expand(key, "EN-AEMK".encode("UTF-8"), length = 16)
    return key


def encrypt_rpi(rpi_key, interval_number):
    enin = encoded_en_interval_number(interval_number)
    padded_data = "EN-RPI".encode("UTF-8") + bytes([0x00] * 6) + enin
    cipher = ucryptolib.aes(rpi_key, 1)
    return cipher.encrypt(padded_data)


def create_list_of_rpis_for_interval_range(rpi_key, interval_number, interval_count, hexlif = False):
	rpis = []
	cipher = ucryptolib.aes(rpi_key, 1)
	padded_data_template = "EN-RPI".encode("UTF-8") + bytes([0x00] * 6)

	if not hexlif:
		for interval in range(interval_number, interval_number+interval_count):
			rpis.append(cipher.encrypt(padded_data_template + encoded_en_interval_number(interval)))
		return rpis
	else: 
		for interval in range(interval_number, interval_number+interval_count):
			rpis.append(ubinascii.hexlify(cipher.encrypt(padded_data_template + encoded_en_interval_number(interval))).decode())
		return rpis


def get_interval_number_from_rpi(rpi, rpi_key):
    cipher = ucryptolib.aes(rpi_key, 1)
    padded_data = cipher.decrypt(rpi)
    assert(padded_data[0:12] == "EN-RPI".encode("UTF-8") + bytes([0x00] * 6))
    return struct.unpack("<I", padded_data[-4:])[0]


def decrypt_aem(aem_key, aem, rpi):
	counter    = Counter(initial_value = int.from_bytes(rpi, "big"))
	aesObject  = AESModeOfOperationCTR(aem_key, counter = counter)
	ciphertext = aesObject.decrypt((bytes(aem))) # try bytes(aem)
	return ciphertext


def encrypt_aem(aem_key, rpi, btmetadata):
	counter = Counter(initial_value = int.from_bytes(rpi, "big"))
	aesObject  = AESModeOfOperationCTR(aem_key, counter = counter)
	ciphertext = aesObject.encrypt((bytes(btmetadata))) # try bytes(aem)
	return ciphertext
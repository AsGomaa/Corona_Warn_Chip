
import utime 
import uasyncio as asyncio
import ujson as json
from os import urandom
from enf.BluetoothService import stop_advertising, start_advertising 
from enf.risk_calculations import file_exists
from enf.newencryption import (generate_tek, derive_rpi_key, derive_aem_key, en_interval_number, 
                               en_interval_number, create_list_of_rpis_for_interval_range, encrypt_aem)


# should be called every 24 hours 
async def schedulingTransmission(allowTransmission = True): # should operate asynchronously
   '''manages the contineous update of the transmitted data'''
   while allowTransmission:
         # Encryption sequence of the Temp. exposeure keys
         tek        = generate_tek(16)       
         rpi_key    = derive_rpi_key(tek)
         aem_key    = derive_aem_key(tek) 
         timestamp  = utime.time() + 946684800 
         enin       = en_interval_number(timestamp)
         rpiList    = create_list_of_rpis_for_interval_range(rpi_key, enin, 144, hexlif = False)
         btmetadata =  bytes([0]) + bytes([0]) + bytes([9]) + bytes([int(64)])
         # Storing the Temp. exposure keys for later upload 
         storeTEK(tek, enin)
         for rpi in rpiList: 
            aem = encrypt_aem(aem_key, rpi, btmetadata)
            transmit(rpi, aem)


async def transmit(rpi, aem, timeInSec=600): # ADDED SYNC 
   '''
   transmits RPI||AEM via bluetooth (the symbol || denotes concatenation)
   time_parameter: time duration for periodic transmission (if a loop is implemented)

   Args:
      rpi (bytes): rolling proximity identifier
      aem (bytes): associated encrypted metadata
      timeInSec (int): the time, in seconds, during which the rpi is valid. 
       
   '''
   # Initialize timer
   start = utime.ticks_ms()
   # Transmitting BT encrypted data indefinitely
   start_advertising(rpi, aem) 
   # Checkes if 10 minutes had passed 
   while utime.ticks_diff(utime.ticks_ms(), start) < (timeInSec * 1000): 
      # ADDED for asynchronous operation
      await asyncio.sleep(0.25) 
   # Stop the BT transmission
   stop_advertising(1)
      

def storeTEK(tek, interval):
   '''stores the passed TEK with the TEKs of the last 14 days
   
   Args:
      tek (bytes): the generated temproray exposure keys
      interval (int): the time interval during which the TEK was generated

   '''
   if file_exists('stored_teks.json'):
        with open('stored_teks.json', 'r') as f:
         # format: storage =  {"tek1": interval1, "tek2": interval2, ...}
            storage = json.load(f) 
            storage[str(tek)] = interval
      # save the stored keys
        update_storage(storage,'stored_teks.json', interval)

   else:
      # initiate the stored_teks.json file for the first time
      storage = {str(tek): interval}
      with open('stored_teks.json', 'w') as f:
            json.dump(storage, f)


def update_storage(storage, filename, enin):
    '''
    delete TEKs that are stored for more than 14 days and stores the the Teks in the file
    filename.

    Args:
      storage (dict): a dictionary containig the the interval numbers as keys and the received encrypted data as value
      {"tek1": interval1, "tek2": interval2, ...}
      filename (str): the name of the file containing the generated TEKs
      enin (int): the current time interval

    '''
    # Selecting the point of time before which the stored data is discarded for being too old
    keep_from = enin - 2016
    # Applying the update criteria 
    storage = dict({ k:v for k, v in storage.items() if (v >  keep_from)})
    with open(filename, 'w') as f:
        json.dump(storage, f)
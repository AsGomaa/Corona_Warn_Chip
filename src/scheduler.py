import utime as time
import uasyncio as asyncio

# to test for instance of coroutine
async def _g():
    pass
type_coro = type(_g())

# scheduledtasks store
scheduledTasks = []

def save( filename='scheduledTasks.json' ):
    """write schedule state to flash"""
    import json
    global scheduledTasks
    with open(filename, 'w') as file:
        json.dump(scheduledTasks, file)

def load( filename='scheduledTasks.json' ):
    """load schedule state from flash"""
    import json 
    global scheduledTasks
    try:
        with open(filename, 'r') as file:
            scheduledTasks = json.load(file)
    except Exception :
        scheduledTasks = []

def clear( filename='scheduledTasks.json' ):
    """clear schedule state to flash"""
    import json
    global scheduledTasks
    with open(filename, 'w') as file:
        json.dump({}, file)
    scheduledTasks = []


def schedule(moduleName, funcName="start", args=(), years=None, weeks=None, days=None, hours=None, minutes=None, seconds=None, times=None):
    """ 
        schedule a function by name to be executed time in the future 
        
        @moduleName String path ans nsame of the module
        @funcName,  String name of the function to call in the module
        @args       Tuple of argument to pass to the function
        @years      Int of years to wait
        @weeks      Int of years to wait
        @days       Int of years to wait
        @hours      Int of years to wait
        @minutes    Int of years to wait
        @seconds    Int of years to wait
        @times      Int of times to repeat the schedule ( None is infinite repeates )
    """

    #time now in seconds since 01.01.2000 
    future = time.time() 

    #calculate the future time
    if years != None:
        future = future + (years * 31556952)

    if weeks != None:
        future = future + (weeks * 604800)

    if days != None:
        future = future + (days * 86400 )
    
    if hours != None:
        future = future + (hours * 3600)

    if minutes != None:
        future = future + (minutes * 60 )

    if seconds != None:
        future = future + seconds

    # add this to the future task
    scheduledTasks.append(( future, moduleName, funcName, args , years, weeks, days, hours, minutes, seconds, times ))
    


async def run():
    """
    execute the task that are ready to run
    """

    global scheduledTasks
    current = time.time() 

    for task in scheduledTasks:  

        # is or was the task ready for execution
        if(  task[0]  < current ):

            future, moduleName, funcName, args , years, weeks, days, hours, minutes, seconds, times = task  
            scheduledTasks.remove( task ) # remove the current task

            if times==None: # re-schedule the future task indefinitely
                schedule(moduleName, funcName, args , years, weeks, days, hours, minutes, seconds, times)

            elif times > 0: # re-schedule the future task untill times expires
                times = times - 1
                if times:
                    schedule(moduleName, funcName, args , years, weeks, days, hours, minutes, seconds, times)

            try:
                print("deep sleep scheduler: task", moduleName, funcName )
                # import the function from the module
                func = getattr( __import__( moduleName.replace(".","/") ), funcName)

                # launch the funciton with args
                res = func( args )

                if isinstance(res, type_coro):
                    asyncio.create_task(res)

            except Exception:
                pass    

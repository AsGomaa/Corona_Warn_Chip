from drivers.ili9341 import img,textBox,brightness,screenColor
from ui.fonts        import Sans18, Sans24, Sans40

def render( batterylevel, batterycharging, batteryfull ,back):
   
    img(0, 0, "bitmaps/background.jpg")
    textBox(90,  60,  "Battery", Sans40 )

    if batterylevel == 25:
        if batterycharging:
            img(145, 120, "bitmaps/bat25.jpg")
            img(95, 115, "bitmaps/charge.jpg")
            textBox(95,175, "Battery charging", Sans18)
        else:
            img(120, 120, "bitmaps/bat25.jpg")
            textBox(100,175, "Please charge !", Sans18)          

    if batterylevel == 50:
        if batterycharging:
            img(145, 120, "bitmaps/bat50.jpg")
            img(95, 115, "bitmaps/charge.jpg")
            textBox(95,175, "Battery charging", Sans18)
        else:
            img(120, 120, "bitmaps/bat50.jpg")
           
    if batterylevel == 75:
        if batterycharging:
            img(145, 120, "bitmaps/bat75.jpg")
            img(95, 115, "bitmaps/charge.jpg")
            textBox(95,175, "Battery charging", Sans18)
        else:
            img(120, 120, "bitmaps/bat75.jpg")        

    if batterylevel == 100  :

        if batteryfull is True: 
            img(120, 120, "bitmaps/bat100.jpg") 
            textBox(115,175, "Battery full", Sans18)
        elif batterycharging is True:
            img(145, 120, "bitmaps/bat100.jpg")
            img(95, 115, "bitmaps/charge.jpg")
            textBox(95,175, "Battery charging", Sans18)
        else:
            img(120, 120, "bitmaps/bat100.jpg") 


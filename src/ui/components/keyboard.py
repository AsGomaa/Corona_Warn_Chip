import machine
from drivers.ili9341 import textBox, screenColor, rect, fillCircle
from drivers.button  import btnA, btnB, btnC
from ui.fonts        import Sans18, Sans24
from ui.colors.healthineers import BACKGROUND, SUCCESS, ERROR, HOVER

from drivers.i2c import i2c
from drivers.mpu6050 import MPU6050

#from power import inactivityTimer
from store.tan import saveTan

uppercase = [
    ["A","B","C","D","E","F","G","H"],
    ["I","J","K","L","M","N","O","P"],
    ["Q","R","S","T","U","V","W","X"],
    ["Y","Z", "","1","2","3","4","5"],
    ["Aa" , "", "","6","7","8","9","0"]
]

lowercase = [
    ["a","b","c","d","e","f","g","h"],
    ["i","j","k","l","m","n","o","p"],
    ["q","r","s","t","u","v","w","x"],
    ["y","z", "","1","2","3","4","5"],
    ["Aa" , "", "","6","7","8","9","0"]
]

characters = uppercase

#globals
keyboardSubmit = None
keyboardCancel = None
keyboardHelp = None
keyboardTimer = None
keyboardValue = ""
keyboardLabel = "Label"
pointer = ( 1,1 )

imu = MPU6050(i2c=i2c)

def render( label, value, submit, cancel , showhelp ):
    global keyboardLabel
    global keyboardValue
    global keyboardSubmit
    global keyboardCancel
    global keyboardHelp
    global keyboardTimer

    keyboardLabel    = label
    keyboardValue    = value or ""
    keyboardSubmit   = submit
    keyboardCancel   = cancel
    keyboardHelp     = showhelp
    
    renderKeynboard()
    #inactivityTimer("taninput")

    #update the pointer position .25 seconds
    keyboardTimer = machine.Timer(4)
    keyboardTimer.init(period=150, mode=keyboardTimer.PERIODIC, callback=updatePointer)


def renderKeynboard():
    """
    this function will draw the keyboard, inputbox, buttons and inital pointer on the screen
    """

    screenColor( BACKGROUND )
    for y in range(len(characters)) :
        for x in range(len(characters[y])):
            renderCharacter( x, y)

    renderInputBox( keyboardValue )
    renderButtons( keyboardValue )
    renderPointer( pointer[0], pointer[1] )

def renderCharacter( x, y ):
    """
    this function will draw an individual character and the surrounding box
    based on the character posinin in the characters matrix
    """

    rect(x*40, y*30, 40, 30, BACKGROUND, 0x000000, borderweight=1)
    textBox((x*40)+10, (y*30)+5, characters[y][x], Sans24) 

def renderPointer( x, y ):
    """
    this function will draw the pointer
    """
    fillCircle( (40*x)+20 , (30*y)+15, 8, 0xFF0000) 

def renderInputBox( keyboardValue ):
    """
    this function will draw the input box its label and value
    """
    textBox(15, 155, keyboardLabel, Sans18 )
    rect(10, 177, 295, 30, BACKGROUND, 0x000000)

    if len(keyboardValue) == 10:
        rect(10, 177, 295, 30, BACKGROUND, SUCCESS)

    textBox(15, 180, keyboardValue, Sans24) 
   
def renderButtons( keyboardValue ):
    """
    this function will draw the buttons based on the length of the keyboard value
    """
    rect(0, 210, 320, 240, BACKGROUND )
    
    if len(keyboardValue) < 10:
        textBox(130,220, "SELECT", Sans18, HOVER) 

    btnB.wasPressed( addCharater )

    if len(keyboardValue):
        btnA.wasPressed( removeCharater )
        btnC.wasPressed( entrySubmit )
        textBox(50,220,  "DEL", Sans18) 

        if len(keyboardValue) == 10:
            textBox(225,220, "SUBMIT", Sans18, SUCCESS) 
        
    else:
        # if there is not keyboardValue, the user can cancel instead of submit
        btnA.wasPressed( entryHelp )
        btnC.wasPressed( entryCancel )
        textBox(45,220,  "HELP", Sans18) 
        textBox(225,220, "CANCEL", Sans18, ERROR) 


def updatePointer( timer ):
    """
    ths function will read the acclerometer and determin the tilt of the screen
    based on the tilt the pointer will be moved
    """
    x,y,z = imu.acceleration

    accelX = x * 100 // 1
    accelY = y * 100 // 1

    global pointer
    posX = pointer[0]
    posY = pointer[1]

    posChanged = False
    
    if accelX > 30 and posX > 0:
        """ step the pointer right """ 
        posX = posX - 1
        posChanged = True

    if accelX < -30 and posX < 7:
        """ step the pointer left """
        posX = posX + 1 
        posChanged = True

    if accelY < -30 and posY > 0:
        """ step the pointer up """
        posY = posY - 1 
        posChanged = True

    if accelY > 30 and posY < 4:
        """ step the pointer down """ 
        posY = posY + 1
        posChanged = True

    if posChanged:
        #inactivityTimer("taninput")
        renderCharacter( pointer[0], pointer[1] )
        renderPointer( posX, posY )
        
    pointer = ( posX, posY )
  
def removeCharater():
    """ remove character fro the textbox """
    global keyboardValue

    if len(keyboardValue):
        keyboardValue = keyboardValue[:-1]
    saveTan( keyboardValue )
    renderButtons( keyboardValue )
    renderInputBox( keyboardValue )
    
    #inactivityTimer("taninput")


def addCharater():
    """ adds the charater at the pointer position to the textbox """
    global keyboardValue
    global characters

    if len(keyboardValue) == 10:
        # dont add more than 10 digits
        return

    if pointer == (0,4):
        # toggles upercase / lowercase
        if characters == uppercase :
            characters = lowercase
        else:
            characters = uppercase

        renderKeynboard()
        return

    # append the character at the pointer position
    keyboardValue = keyboardValue + characters[pointer[1]][pointer[0]]
    saveTan( keyboardValue )
    renderButtons(keyboardValue ) 
    renderInputBox( keyboardValue ) 
    #inactivityTimer("taninput")

def entrySubmit():
    """ stop the pointer update timer and return keyboardValue """
    if len(keyboardValue) == 10:
        keyboardTimer.deinit()
        keyboardSubmit( keyboardValue )
        saveTan( keyboardValue )

def entryCancel():
    """ stop the pointer update timer and cancels"""
    keyboardTimer.deinit()
    keyboardCancel()
   
def entryHelp():
    """ stop the pointer update timer and shows the keyboard help"""
    keyboardTimer.deinit()
    keyboardHelp()
    

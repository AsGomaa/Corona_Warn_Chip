from drivers.ili9341 import img,textBox,brightness
from drivers.button  import btnB
from ui.fonts        import  Sans18, Sans24, Sans40
from ui.colors.healthineers import BACKGROUND 
#from power import inactivityTimer

def render( back ):

    brightness(100)
    img(0, 0, "bitmaps/background.jpg")
    textBox(55, 80, "Thank You!", Sans40 )

    textBox(70,140, "Get well soon", Sans24)

    textBox(145,210,  "OK", Sans18)    
    btnB.wasPressed( back )

    #inactivityTimer("tanverified")

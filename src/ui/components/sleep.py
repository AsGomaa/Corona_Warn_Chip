import utime
from drivers.ili9341 import img, brightness

def render():
    brightness(100)
    img(0, 0, "bitmaps/sleep.jpg")
    # dim the screen
    for i in range(100):
        brightness(100-i)
        utime.sleep_ms(50)
    

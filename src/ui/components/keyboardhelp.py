from drivers.ili9341 import img, textBox 
from drivers.button  import btnA, btnB, btnC
from ui.fonts        import Sans18, Sans24
from ui.colors.healthineers import BACKGROUND 
#from power import inactivityTimer

def render( back ):

    img(0, 0, "bitmaps/background.jpg")
    textBox(65,65, "Keyboard Help", Sans24)
    textBox(35,105,"Move the red cursor by tilting", Sans18 )
    textBox(35,125,"the screen. SELECT a character", Sans18 )
    textBox(35,145,"Enter the 10 Digit Tan", Sans18)
    textBox(35,165,"press SUBMIT to verify", Sans18)
    textBox(45,210,"BACK", Sans18) 

    #inactivityTimer("taninputhelp")    
    btnA.wasPressed( back )
    
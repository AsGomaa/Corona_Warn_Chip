from drivers.ili9341 import img,textBox,brightness
from ui.fonts        import Sans18, Sans24, Sans40
from drivers.button  import btnC

def render(exposure, back):
    brightness(100)
    img(0, 0, "bitmaps/background.jpg")
    
    print("exposure = " + str(exposure))
    textBox(230,210, "BACK",  Sans18)
    textBox(70,55, "Exposure",  Sans40)
    textBox(120,105, "Today " + str(exposure[1]),  Sans24)
    textBox(80,135,"Last 14 days " + str(exposure[0]) , Sans24)


    btnC.wasPressed( back )

    
    

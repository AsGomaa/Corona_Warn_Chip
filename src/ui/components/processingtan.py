import utime
import uasyncio
from drivers.ili9341 import img,textBox,brightness, screenColor, rect
from drivers.button  import btnA, btnB, btnC
from ui.fonts        import Sans18, Sans24, Sans40
from ui.colors.healthineers import BACKGROUND, SUCCESS, WARNING, ERROR 
#from power import inactivityTimer

def render( tan, thankyou, incorrect ):

    brightness(100)
    img(0, 0, "bitmaps/background.jpg")
    textBox(15, 60, "Processing Tan", Sans40 )

    textBox(20,120, "Verifyting TAN", Sans24)
    textBox(20,160, "Data Transfer", Sans24)
    # Asyncronous Task
    async def showProgress():
        try:
            await uasyncio.sleep(2)
            img(210, 118, "bitmaps/success.jpg")
            await uasyncio.sleep(2)
            img(210, 158, "bitmaps/success.jpg")
            await uasyncio.sleep(2)
            thankyou()
        except:
            pass

    # Que the Task for execution
    uasyncio.run(showProgress())

    btnC.wasPressed( incorrect )

    #inactivityTimer("tansending")
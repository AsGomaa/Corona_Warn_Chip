from drivers.ili9341 import img,textBox,brightness
from drivers.button  import btnB, btnC
from ui.fonts        import Sans18, Sans24, Sans40
#from power          import inactivityTimer

def render( riskstartdate, batterylevel, risk, exposure ):

    brightness(100)
    img(0, 0, "bitmaps/background.jpg")
    textBox(70,  60,  "Welcome", Sans40 )
    textBox(55,  110, "CORNA Warn Chip", Sans24 )
    textBox(15,  140, "has been working for you", Sans24 )
    textBox(55,  170, "since "+riskstartdate, Sans24)

    textBox(125,210, "MY RISK",  Sans18) 
    textBox(225,210, "EXPOSURE",  Sans18) 
    
    btnB.wasPressed( risk )
    btnC.wasPressed( exposure )




from drivers.ili9341 import img, textBox, brightness, screenColor, rect
from drivers.button  import btnA, btnB
from ui.fonts        import Sans18, Sans24, Sans40
from ui.colors.healthineers import BACKGROUND, SUCCESS, WARNING 
#from power import inactivityTimer

def render( risklevel, timestamp, keyboard ):

    screenColor( BACKGROUND )
    brightness( 100 )

    img(0, 0, "bitmaps/background.jpg")
    textBox( 65, 60, "Risk Level", Sans40 ) 

    if risklevel == 0: # No Risk Level
        textBox( 10, 110, "is'nt ready at the moment", Sans24 )
        textBox( 10, 130, "please wait a few days ", Sans24 )
        textBox( 10, 150, "untill there is enough data ", Sans24 )
        textBox( 10, 170, "to complete a risk analysis", Sans24 )

    if risklevel == 1:
        rect(55, 110, 205, 50, SUCCESS, SUCCESS)
        textBox(110, 117, "LOW", Sans40, bgcolor=SUCCESS)
        textBox(75,180, "as of "+timestamp, Sans18)
    
    if risklevel == 2:
        rect( 55, 110, 205, 50, WARNING, WARNING )
        textBox(105, 117, "HIGH", Sans40, bgcolor=WARNING )
        textBox(75,170, "as of "+timestamp, Sans18)
        textBox(25,195, "Get tested for COVID-19", Sans24)

    textBox(120,210, "Enter TAN",  Sans18) 

    #inactivityTimer("risk")
    btnB.wasPressed( keyboard )


 


    
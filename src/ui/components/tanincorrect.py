from drivers.ili9341 import img,textBox,brightness
from drivers.button  import btnC
from ui.fonts        import  Sans18, Sans24,
from ui.colors.healthineers import BACKGROUND 
#from power import inactivityTimer


def render( keyboard ):

    brightness(100)
    img(0, 0, "bitmaps/background.jpg")

    img(40, 108, "bitmaps/warning.jpg")
    textBox(85, 108,'TAN Incorrect', Sans24)
    textBox(35, 138,'Please check your TAN', Sans24)

    textBox(233,210,  "BACK", Sans18)
    btnC.wasPressed( keyboard )
    #inactivityTimer("tanincorrect")
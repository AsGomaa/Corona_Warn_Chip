import machine
import ui.components.keyboard as keyboard
from ui.router import transition
from store.tan import loadTan 

"""
Tan Input Page
"""

def init( params ):
    """ render the keyboard """  
    machine.freq(240000000) # bump up the performance for this part
    keyboard.render( "please enter 10 digit Tan:", loadTan() , submit, back, showhelp )

def submit( tan ):
    machine.freq(160000000) # back to normal performance
    transition( 'processingtan', tan )

def showhelp():
    machine.freq(160000000) # back to normal performance
    transition( 'keyboardhelp' )

def back():
    machine.freq(160000000) # back to normal performance
    transition( 'risk' )


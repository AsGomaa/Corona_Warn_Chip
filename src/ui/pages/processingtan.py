import ui.components.processingtan as processingtan
from ui.router import transition
from store.tan import clearTan 

"""
Tan Sending Page
"""

def init( tan ):
    """render Tan sending page"""
    processingtan.render( tan, thankyou, incorrect )

def thankyou():
    """transition to thankyou""" 
    clearTan()
    transition( "thankyou" )   
    
def incorrect():
    """transition to tanincorrect""" 
    transition( "tanincorrect" ) 
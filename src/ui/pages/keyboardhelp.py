import ui.components.keyboardhelp as keyboardhelp
from ui.router import transition

"""
Tan Input Help Page
"""

def init( params ):
    """ render the keyboard """  
    keyboardhelp.render( back )

def back():
    transition( "keyboard" )

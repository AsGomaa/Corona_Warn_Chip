

import ui.components.welcome as welcome
import ui.components.battery as battery

from store.risk     import startdateString
from store.battery  import batterylevel, batterycharging, batteryfull
from ui.router      import transition
from machine        import Timer


"""
Welcomes the user
"""

timer = Timer(6)

def init( params ):
    """render welcome page"""
    welcome.render( startdateString(), batterylevel(), risk, exposure )

    # if the user does not press a buttin in 5seconds the device transitions to batterystatus
    timer.init(period=5000, mode=timer.ONE_SHOT, callback=batteryStatus)

def risk():
    """transition to risk page""" 
    timer.deinit()
    transition( "risk" )

def exposure():
    """transition to keyboard page for tan input""" 
    timer.deinit()
    transition( "exposure" )  

def batteryStatus( timer ):
    transition( "battery" , "welcome" ) 


    


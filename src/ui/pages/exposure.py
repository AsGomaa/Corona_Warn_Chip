import ui.components.exposure as exposure
from ui.router import transition
from store.exposure import save, load

"""
Exposure Page
"""

def init( params ):
    save()
    exposure.render(load(), back)


def back( ):
    transition( "welcome" , None ) 
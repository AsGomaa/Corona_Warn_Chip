import ui.components.battery as battery
from ui.router import transition
from store.battery import batterylevel, batterycharging, batteryfull
from machine import Timer

"""Shows the State of the Battery"""

timer = Timer(6)

def init( previousstate ):
        
    battery.render( batterylevel(), batterycharging(), batteryfull(), back( previousstate ) )
    
    timer.init(period=5000, mode=timer.ONE_SHOT, callback=sleep( previousstate )) 


def back( previousstate ):
    """transition to risk"""
    def closure():
            timer.deinit() 
            transition( previousstate )
    return closure  


def sleep( previousstate ):
    """transition to sleep"""
    def closure( timer ):
        transition("sleep", previousstate)
    return closure    
import ui.components.tanincorrect as tanincorrect
from ui.router import transition

"""
Tan Incorrect Page
"""

def init( params ):
    tanincorrect.render( keyboard )
    
def taninput():
    """transition to keyboard for TAN correction"""
    transition( "keyboard" )   

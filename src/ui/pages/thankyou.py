import ui.components.thankyou as thankyou
from ui.router import transition
from machine import Timer

"""
Tan verification and data upload sucessfull
"""
timer = Timer(6)

def init( params ):
    """render thankyou component"""
    thankyou.render( risk )
    timer.init(period=5000, mode=timer.ONE_SHOT, callback=batteryStatus) 

def risk():
    """transition to risk"""
    timer.deinit() 
    transition( "risk" )

def batteryStatus( timer ):
    """transition to battery"""
    transition( "battery" , "thankyou")       
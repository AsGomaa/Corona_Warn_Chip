import power
import ui.components.sleep as sleep

"""
Power saving page
"""

def init( wakestate ):
    """
    @wakestate: string name of the page to show when
    the devices wakes up by a button press
    """
    sleep.render()
    power.deepsleep( wakestate )



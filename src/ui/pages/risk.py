from ui.router import transition
from store.risk import risk, timestampString
#from store.battery import batterylevel, batterycharging, batteryfull

import ui.components.risk as riskview
import ui.components.battery as battery
from machine import Timer


"""
Shows risk level to the user
"""
timer = Timer(6)

def init( params ):
    """render risk"""
    riskview.render( risk(), timestampString(), keyboard )  
    timer.init(period=5000, mode=timer.ONE_SHOT, callback=batteryStatus)

   
def keyboard():
    """ transition to eyboard for TAN entry """
    timer.deinit() 
    transition( "keyboard" )

def back():
    """ back to welcome page """
    timer.deinit()
    transition( "welcome" )

def batteryStatus( timer ):
    transition( "battery" , "risk")     

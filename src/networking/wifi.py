import utime
import network 
from store.wifi import ssid, password

class WifiError(Exception):
      pass

# join a wifi access point
def joinwifi():
    """ join a wifi access point """

    retryTime = 15 # amount of time to establish a connection

    # deactivating access point mode
    ap = network.WLAN(network.AP_IF)
    ap.active(False)

    # initiate a station mode
    station = network.WLAN(network.STA_IF) 

    if not station.isconnected():
            print('connecting to network:', ssid())
            station.active(True)
            # station.connect(ssid(), password())
            station.connect('SummerTime', 'Calmhat436')
            # station.connect('KabelBox-C954', '66476179533635517536')

            while not station.isconnected():
                utime.sleep(1)
                retryTime = retryTime - 1
                if retryTime <= 0:
                    station.disconnect()
                    station.active(False)
                    raise WifiError("Unable to join WiFi: ", ssid()) 
                
    ip = station.ifconfig()[0]
    print('connected as:', ip)

    return ip

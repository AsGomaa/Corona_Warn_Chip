import urequests
import uio
import utime
from micropython import const
from networking.wifi import joinwifi
from store.risk import timestamp
chunksize = const(1024)

def downloadfile(path):
    """
    downloads a file from a url directly to the sdcard
    @url  String URL of the file to download
    @path String path and filename on the sdcard
    """
    joinwifi() #TODO: change to LTE
    root    = "https://ctt.pfstr.de/json/"
    newfile = name_update()
    url     = root + newfile
    sock    = urequests.get( url ).raw
    # path = "teks.json" #name_update()
    with uio.open( path, 'wb' ) as f:
        while True:
            chunk = sock.read( chunksize )
            if not chunk:
                break
            print(".")
            f.write( chunk )
        f.close()
    


def name_update():
    '''
    Returns a string of the updated file name containing uploading diagnosis keys
    '''
    url = "https://svc90.main.px.t-online.de/version/v1/diagnosis-keys/country/DE/date"

    try:
        response = urequests.get(url).json()
    except Exception as e:
        return False
    else:
        return str(response[-1]) + '.json'

